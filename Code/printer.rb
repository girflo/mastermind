# Module      : Printer
# Use        : This module is called every time the script need to print an
#              information to the user.
module Printer
  # Use    : Print the message for choosing a combination and the rules if
  #          needed
  # Input  : marbles     : Int : Number of needed marbles in the combination
  #          print_rules : Boolean : Define if the script need to print the
  #                                  rules
  # Output : nil
  def self.combination_choice(marbles, print_rules)
    puts "Choose a new combination of #{marbles} marbles, duplicate are "\
          'not allowed in the secret combination :'
    line
    return unless print_rules
    rule_color_choice
    rule_combination_format(marbles)
  end

  # Use    : Print the secret combination
  # Input  : String : The content of the secret combination
  # Output : nil
  def self.combination(combination)
    puts "The secret combination is : #{combination}"
    line
  end

  # Use    : Print the defeat message
  # Input  : nil
  # Output : nil
  def self.defeat_message
    puts "You didn't succeded to find the secret combination, maybe next time."
    line
  end

  # Use    : Print the difficulty level
  # Input  : String : The difficulty level
  # Output : nil
  def self.difficulty(difficulty)
    line
    puts 'The difficulty for this game is set to ' + difficulty
    line
  end

  # Use    : Print the difficulty choice
  # Input  : nil
  # Output : nil
  def self.difficulty_choice
    puts 'Choose the difficulty level number :'
    puts '1 - Easy'
    puts '2 - Medium'
    puts '3 - Hard'
    line
  end

  # Use    : Print a line
  # Input  : nil
  # Output : nil
  def self.line
    puts '---------------------------------------------------------'
  end

  # Use    : Print an array with all the combinations and their accuracy
  # Input  : Array : Combination : All combinations input by the user
  # Output : nil
  def self.result(user_combinations)
    line
    puts 'Result :'
    result_array_line
    puts'| Try number | Combination colors | Combination accuracy |'
    result_array_line
    result_array(user_combinations)
    result_array_line
    puts '0 = Correct | . = Present but elsewhere | x = Absent'
    line
  end

  # Use    : Print the content of the result array
  # Input  : Array : Combination : Every combination input by the user
  # Output : nil
  def self.result_array(user_combinations)
    space_size = result_array_space_size(user_combinations.first.colors.size)
    user_combinations.each_with_index do |combination, i|
      result_array_info_line(i, space_size, combination.colors_to_string,
                             combination.accuracy_to_string)
    end
  end

  # Use    : Print a result line for one combination
  # Input  : i        : Int    : The guess number
  #          spaces   : Int    : Number of spaces needed to centered the content
  #          colors   : String : The colors contained in the combination
  #          accuracy : String : The accuracy of the combination
  # Output : nil
  def self.result_array_info_line(i, spaces, colors, accuracy)
    puts result_array_info_line_index(i) +
         result_array_info_line_colors(spaces, colors) +
         result_array_info_line_accuracy(spaces, accuracy)
  end

  # Use    : Print the accuracy part of the result line for one combination
  # Input  : spaces   : Int    : Number of spaces needed to centered the content
  #          accuracy : String : The accuracy of the combination
  # Output : nil
  def self.result_array_info_line_accuracy(spaces, accuracy)
    (' ' * (spaces + 2)) + accuracy + (' ' * (spaces + 1)) + '|'
  end

  # Use    : Print the color part of the result line for one combination
  # Input  : spaces   : Int    : Number of spaces needed to centered the content
  #          colors   : String : The colors contained in the combination
  # Output : nil
  def self.result_array_info_line_colors(spaces, colors)
    (' ' * (spaces + 1)) + colors + (' ' * spaces) + '|'
  end

  # Use    : Print the index part of the result line for one combination
  # Input  : i        : Int    : The guess number
  # Output : nil
  def self.result_array_info_line_index(i)
    "|    #{i}/8     |"
  end

  # Use    : Print the line for the result array
  # Input  : nil
  # Output : nil
  def self.result_array_line
    puts '|------------|--------------------|----------------------|'
  end

  # Use    : Determine the number of spaces needed to center the content,
  #          depending on the size of the combination.
  # Input  : Int : The size of the combination
  # Output : Int : The number of space needed
  def self.result_array_space_size(combination_size)
    if combination_size == 6
      4
    elsif combination_size == 5
      5
    else
      6
    end
  end

  # Use    : Print the available colors for creating a combination
  # Input  : nil
  # Output : nil
  def self.rule_color_choice
    puts 'Available colors :'
    i = 0
    while i < COLORS.size
      puts COLORS[i] + ' - ' + COLORS_NAMES[i]
      i += 1
    end
    line
  end

  # Use    : Print the format needed for creating a combination
  # Input  : Int : Number of marbles neede in the game
  # Output : nil
  def self.rule_combination_format(marbles)
    puts 'Needed format :'
    marbles.times do
      print 'x '
    end
    puts "\n" + 'With x the initial of the color.'
    line
  end

  # Use    : Print the victory message
  # Input  : nil
  # Output : nil
  def self.victory_message
    abort('Congratulation you found the secret combination.')
  end
end
