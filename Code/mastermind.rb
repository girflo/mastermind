# MASTERMIND
require './classes/combination.rb'
require './classes/difficulty.rb'
require './classes/game.rb'
require './classes/marble.rb'
require './classes/secret_combination.rb'
require './printer.rb'

# Global constants
# Available colors for combination :
#   g : green
#   r : red
#   b : blue
#   y : yellow
#   w : white
#   o : orange
# The arrays are frozen as they are stored in constants therefore they could not
# be changed
COLORS = %w(g r b y w o p).freeze
COLORS_NAMES = %w(green red blue yellow white orange purple).freeze

# Available difficulty for the game :
#   1 : Easy
#   2 : Medium
#   3 : Hard
DIFFICULTIES = [1, 2, 3].freeze

# Begining of the script
current_game = Game.new
secret_combination = SecretCombination.new(current_game.difficulty.marbles)
Printer.line

# This function show the content of the secret combination which can be useful
# for testing the script
# Printer.combination(secret_combination.colors.join(' '))

# The user only has nine guess to find the secret combination
9.times do |i|
  # For the first time we want to print the rules, after that it became
  # redundant
  print_info = i.zero? ? true : false
  current_user_combination = Combination.new(current_game.difficulty.marbles,
                                             print_info)
  # The script test the accuracy of the new combination with the secret one
  current_user_combination.test_combination(secret_combination)
  # Add the current combination to all combinations to print them in the result
  # array
  current_game.add_user_combination(current_user_combination)
  Printer.result(current_game.user_combinations)
  # The script check if the user has guessed the secret combination in order to
  # print the correct message and stop.
  current_user_combination.check_for_victory
end
Printer.defeat_message
