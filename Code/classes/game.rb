# Class      : Game
# Use        : Define the current game
# Attributes :
#   difficulty        : Difficulty : current game's difficulty
#   user_combinations : Array      : All combinations input by the user
class Game
  # Getter
  attr_reader :difficulty

  # Accessor
  attr_accessor :user_combinations

  # Constructor
  # Input  : nill
  # Output : Game : The newly created game with the difficulty set by the user
  def initialize
    @difficulty = Difficulty.new(difficulty_choice)
    @user_combinations = []
    Printer.difficulty(@difficulty.name)
  end

  # Use    : Add the current combination to the past one
  # Input  : Combination : The current combination
  # Output : nil
  def add_user_combination(current_combination)
    @user_combinations << current_combination
  end

  private

  # Use    : Ask and set the difficulty level wanted by the user
  # Input  : nill
  # Output : String : Current difficulty string input by the user
  def difficulty_choice
    Printer.line
    Printer.difficulty_choice
    # Store the user input and change it's class to integer
    user_difficulty = gets.chomp.to_i
    return user_difficulty if DIFFICULTIES.include? user_difficulty
    puts 'You should enter 1, 2 or 3.'
    difficulty_choice
  end
end
