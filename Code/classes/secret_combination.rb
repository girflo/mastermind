# Class      : SecretCombination
# Infos      : Child of Class Combination
# Use        : Create the secret combination needed to be find by the user
# Attributes :
#   marbles_set : Array      : 4 to 6 Class Marbles depending of game's
#                              difficulty
#   difficulty  : Difficulty : cf. Difficulty Class
class SecretCombination < Combination
  # Constructor
  # Input  : Int               : Number of marbles needed in the combination
  # Output : SecretCombination : The newly generated secret combination
  def initialize(marbles)
    random_marbles_set = []
    COLORS.sample(marbles).each do |color|
      random_marbles_set << Marble.new(color)
    end
    @marbles_set = random_marbles_set
  end
end
