# Class      : Difficulty
# Use        : define the game difficulty
# Attributes :
#   level   : Integer : From 1 to 3, depending of the user input
#   marbles : Integer : From 4 to 6, total number of marbles contained in the
#                       secret combination
#   name    : String  : Easy, Medium or Hard.
class Difficulty
  # Getters
  attr_reader :level, :marbles, :name

  # Constructor
  # Input  : Int        : The difficulty level input by the user
  # Output : Difficulty : The newly created difficulty choosed by the user
  def initialize(difficulty_level)
    case difficulty_level
    when 1
      easy
    when 2
      medium
    when 3
      hard
    end
  end

  private

  # Use    : Set the correct attributes for an easy game
  # Input  : nill
  # Output : nill
  def easy
    @level = 1
    @marbles = 4
    @name = 'Easy'
  end

  # Use    : Set the correct attributes for a medium game
  # Input  : nill
  # Output : nill
  def medium
    @level = 2
    @marbles = 5
    @name = 'Medium'
  end

  # Use    : Set the correct attributes for a hard game
  # Input  : nill
  # Output : nill
  def hard
    @level = 3
    @marbles = 6
    @name = 'Hard'
  end
end
