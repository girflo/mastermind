# Class      : Combination
# Use        : creating a combination of marbles with user input
# Attributes :
#   marbles_set : Array : 4 to 6 Marbles depending of game's difficulty
#   accuracy    : Array : 4 to 6 characters ('.', '0' or 'x') that defined the
#                         correctness of each marble color with the secret
#                         combination.
class Combination
  # Accessors
  attr_accessor :marbles_set, :accuracy

  # Use    : Return the accuracy of each color concatenated in a string
  #          seperated by a space
  # Input  : nill
  # Output : String : Accuracy of each color
  def accuracy_to_string
    @accuracy.join(' ')
  end

  # Use    : Check if the user discovered the secret combination via a regex
  # Input  : String : User combination
  # Output : Bool   : True  : The user find the secret combination
  #                   False : The user didn't find the secret combination yet
  def check_for_victory
    return unless accuracy_to_string =~ /^(0\s){3,5}0$/
    Printer.victory_message
  end

  # Use    : Return an array containing the color for each marble contained in
  #          marbles_set
  # Input  : nill
  # Output : Array : String : Color of each marble stored in marbles_set
  def colors
    @marbles_set.map(&:color)
  end

  # Use    : Return the colors of each marble concatenated in a string seperated
  #          by a space
  # Input  : nill
  # Output : String : Color of each marble stored in marbles_set
  def colors_to_string
    colors.join(' ')
  end

  # Use    : Change the user's inputed string in a combination
  # Input  : marbles     : Int : Number of needed marbles in the combination
  #          print_rules : Boolean : Define if the script need to print the
  #                                  rules
  # Output : Combination : The new combination
  def initialize(marbles, print_rules)
    # In order to create the marble set of this combination we are asking for
    # the user to input a string.
    user_combination_string = user_combination_choice(marbles, print_rules)
    # Once we have a syntactically checked combination string, this part of the
    # script split the string and create an array including only colors' first
    # letter. For each of those colors we are creating a marble and store it in
    # an array. Finally the array containing all the marbles is stored as the
    # marble set of this combination.
    @marbles_set = user_combination_string.split(' ')
                                          .map { |color| Marble.new(color) }
  end

  # Use    : Compare the user combination with the secret combination, then set
  #          the accuracy of the combination
  # Input  : Combination : The secret combination
  # Output : nill
  def test_combination(secret_combination)
    user_combination_accuracy = []
    @marbles_set.each_with_index do |marble, i|
      if marble.color == secret_combination.colors[i]
        user_combination_accuracy << '0'
      elsif secret_combination.colors.include?(marble.color)
        user_combination_accuracy << '.'
      else user_combination_accuracy << 'x'
      end
    end
    @accuracy = user_combination_accuracy
  end

  private

  # Use    : Ask, retrieve and test via a regex the user combination
  # Input  : marbles    : Integer : Number of marbles needed depending of the
  #                                 difficulty
  #         print_rules : Bool    : True  : Print the rules
  #                                 False : Don't print the rules
  # Output : String  : The user's combination
  def user_combination_choice(marbles, print_rules)
    Printer.combination_choice(marbles, print_rules)
    user_combination = gets.chomp
    combination_regex = %r/([#{COLORS.join('')}]\s)
                            {#{marbles.to_int - 1}}
                            [#{COLORS.join('')}]/x
    return user_combination if user_combination.match combination_regex
    user_combination_choice(marbles, true)
  end
end
