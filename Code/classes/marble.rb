# Class      : Marble
# Use        : Define a marble contained in a combination
# Attributes :
#   color : String : cf. COLORS Constant
class Marble
  # Getter
  attr_reader :color

  # Constructor
  # Input  : String : First letter of the marble color
  # Output : Marble : The newly created marble with the color set by the user
  #          nil    : If the color is incorrect
  def initialize(color)
    return unless COLORS.include? color
    @color = color
  end
end
