# Mastermind

Language : Ruby // This software is a small mastermind in console done in ruby.

## Description
The project is a game of mastermind in console. The goal of this game is to discover a combination of several marble chosen by the script among the following colors :
  * g : green
  * r : red
  * b : blue
  * y : yellow
  * w : white
  * o : orange

There are three level of difficulty :

| Level | Marble to discover |
| ----- | ------------------ |
| Easy | 4 |
| Medium | 5 |
| Hard | 6 |

The user has 8 guesses to find the correct combination, each time the script will return one of those three symbols for each marble :

| Symbol | Meaning |
| ------ | ------- |
| . | The marble is present in the combination but in an other place |
| 0 | The marble is at this place |
| x | The marble isn't in the secret combination |

## Screenshots

### Difficulties
#### Menu
![mastermind difficulty menu](img/difficulty_menu.png)
#### Easy
![mastermind difficulty easy](img/easy.png)
#### Medium
![mastermind difficulty medium](img/medium.png)
#### Hard
![mastermind difficulty hard](img/hard.png)

### Guess
![mastermind guess](img/guess.png)

### Win
![mastermind win](img/win.png)

### Loose
![mastermind loose](img/loose.png)
